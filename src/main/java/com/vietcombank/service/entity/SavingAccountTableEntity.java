package com.vietcombank.service.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import java.time.Instant;
import java.util.UUID;

import java.math.BigDecimal;

@Entity
@Table(name = "SAVING_ACCOUNTS")

public class SavingAccountTableEntity {
    // @Id
    // @Column(name = "id", nullable = false, unique = true)
    // private UUID id;
    @Id
    @Column(name = "ACCOUNTNO", nullable = false, unique = true)
    private int accountNo;

    @Column(name = "BALANCE", nullable = true)
    private BigDecimal balance;

    @Column(name = "CREATEDDATE", nullable = true)
    private Instant createdDate;

    @Column(name = "CLOSEDDATE", nullable = true)
    private Instant closedDate;

    @Column(name = "INTERESTSTARTDATE", nullable = true)
    private Instant interestStartDate;

    @Column(name = "MATURITYDATE", nullable = true)
    private Instant maturityDate;

    @Column(name = "TYPE", nullable = true)
    private String type;

    @Column(name = "STATUS", nullable = true)
    private String status;

    @Column(name = "CUSTOMERCIFNO", nullable = true)
    private int customerCifNo;

    public SavingAccountTableEntity() {
        // this.id = UUID.randomUUID();
    };

    public SavingAccountTableEntity(int accountNo, BigDecimal balance, Instant createdDate, Instant closedDate, String type, int customerCifNo) {
        // SavingAccountTableEntity savingAccountTableEntity = new SavingAccountTableEntity();
        // this.id = UUID.randomUUID();
        this.accountNo = accountNo;
        this.balance = balance;
        this.createdDate = createdDate;
        this.closedDate = closedDate;
        this.type = type;
        this.customerCifNo = customerCifNo;
    }

    // public void setId(UUID id) {
    //     this.id = id;
    // }

    // public UUID getId() {
    //     return this.id;
    // }

    public int getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(int acc) {
        this.accountNo = acc;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getBalance() {
        return this.balance;
    }

    public void setCreatedDate(Instant date) {
        this.createdDate = date;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public void setClosedDate(Instant date) {
        this.closedDate = date;
    }

    public Instant getClosedDate() {
        return this.closedDate;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCustomerCifNo() {
        return this.customerCifNo;
    }

    public void setCustomerCifNo(int cif) {
        this.customerCifNo = cif;
    }
}