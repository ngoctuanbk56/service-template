package com.vietcombank.service.entity;
import java.math.BigDecimal;

public class SavingUpdateRequest {
    private int accNo;
    private BigDecimal balance;

    public void setAccNo(int acc) {
        this.accNo = acc;
    }
    public int getAccNo() {
        return this.accNo;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getBalance() {
        return this.balance;
    }
}