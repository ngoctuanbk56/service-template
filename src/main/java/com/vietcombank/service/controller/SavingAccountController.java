package com.vietcombank.service.controller;

import com.vietcombank.service.service.SavingAccountService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vietcombank.service.entity.SavingAccountTableEntity;
import com.vietcombank.service.entity.SavingUpdateRequest;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/restapi")
public class SavingAccountController {
    @Autowired
    private SavingAccountService savingAccountService;

    @GetMapping(value = "/v1/savingaccounts", produces = "application/json")
    @Operation(summary = "Retrieve All saving accounts")
    public ResponseEntity<?> getAllSavingAccounts() {
        log.info("retrieve all saving accounts");
        return ResponseEntity.ok(savingAccountService.retrieveAll());
    }

    @PostMapping(value = "/v1/savingaccounts")
    @Operation(summary = "Create a saving account")
    public ResponseEntity<?> createSavingAccounts(@RequestBody SavingAccountTableEntity acc) {
        // UUID uuid = UUID.randomUUID();
        // SavingAccountTableEntity newAcc = new SavingAccountTableEntity(
        //     uuid, 
        //     acc.getAccountNo(),
        //     acc.getBalance(),
        //     acc.getCreatedDate(),
        //     acc.getClosedDate(),
        //     "1111",
        //     acc.getCustomerCifNo()
        // );
        return ResponseEntity.ok(savingAccountService.saveAccount(acc));
    }
    @PutMapping(value = "/v1/savingaccounts/{accNo}")
    @Operation(summary = "update a saving account")
    public ResponseEntity<?> updateSavingAccount(@RequestBody SavingUpdateRequest acc) {
        
        return ResponseEntity.ok(savingAccountService.updateAccount(acc));
    }

    @GetMapping(value = "/v1/savingaccounts/{accNo}")
    @Operation(summary = "Get a saving account")
    public ResponseEntity<?> getSavingAccount(@PathVariable("accNo") int accNo) {
        
        return ResponseEntity.ok(savingAccountService.getAccount(accNo));
    }

    @DeleteMapping(value = "/v1/savingaccounts/{accNo}")  
    @Operation(summary = "Delete a saving account")
    public ResponseEntity<?> deleteSavingAccount(@PathVariable("accNo") int accNo) {
        
        return ResponseEntity.ok(savingAccountService.deleteAccount(accNo));
    } 
}