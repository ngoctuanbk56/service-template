package com.vietcombank.service.service;

import com.vietcombank.service.entity.SampleTableEntity;
import com.vietcombank.service.repository.SavingAccountTableEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vietcombank.service.entity.SavingAccountTableEntity;
import com.vietcombank.service.entity.SavingUpdateRequest;

import java.util.List;

@Slf4j
@Service
public class SavingAccountService {
    @Autowired
    private SavingAccountTableEntityRepository savingAccountTableEntityRepository;

    public List<SavingAccountTableEntity> retrieveAll() {
        return savingAccountTableEntityRepository.findAll();
    }

    public SavingAccountTableEntity saveAccount(SavingAccountTableEntity acc) {
        return savingAccountTableEntityRepository.save(acc);
    }

    public SavingAccountTableEntity updateAccount(SavingUpdateRequest acc) {
        SavingAccountTableEntity savingAccountTableEntity = savingAccountTableEntityRepository.findByAccountNo(acc.getAccNo());
        savingAccountTableEntity.setBalance(acc.getBalance());
        savingAccountTableEntityRepository.save(savingAccountTableEntity);
        return savingAccountTableEntity;
    }

    public SavingAccountTableEntity getAccount(int acc) {
        SavingAccountTableEntity savingAccountTableEntity = savingAccountTableEntityRepository.findByAccountNo(acc);
        return savingAccountTableEntity;
    }

    public SavingAccountTableEntity deleteAccount(int acc) {
        SavingAccountTableEntity savingAccountTableEntity = savingAccountTableEntityRepository.findByAccountNo(acc);
        savingAccountTableEntityRepository.delete(savingAccountTableEntity);
        return savingAccountTableEntity;
    }
}
