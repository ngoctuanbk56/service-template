package com.vietcombank.service.repository;

import com.vietcombank.service.entity.SavingAccountTableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface SavingAccountTableEntityRepository extends JpaRepository<SavingAccountTableEntity, UUID> {
    SavingAccountTableEntity findByAccountNo(int accNo);
}