--liquibase formatted sql
--changeset microservice_class:20230725_1

CREATE
EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE SAMPLE_TABLE
(
    ID         uuid PRIMARY KEY default uuid_generate_v4(),
    PRODUCT    varchar(255),
    UPDATED_AT timestamp
);

CREATE TABLE SAVING_ACCOUNTS
(
    -- id uuid PRIMARY KEY default uuid_generate_v4(),
    ACCOUNTNO numeric(10,0) PRIMARY KEY,
    BALANCE numeric(19,0),
    CREATEDDATE timestamp,
    CLOSEDDATE timestamp,
    INTERESTSTARTDATE timestamp, 
    MATURITYDATE timestamp,
    TYPE varchar(10),
    STATUS varchar(5),
    CUSTOMERCIFNO numeric(10,0)
);