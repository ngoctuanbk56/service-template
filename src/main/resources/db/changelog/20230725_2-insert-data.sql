--liquibase formatted sql
--changeset microservice_class:20230725_2

INSERT INTO SAMPLE_TABLE
(id, product, updated_at)
VALUES ('6364bf5d-fa89-4ab3-b274-1da8cdb785b3', 'product-1', '2023-07-25 11:34:53.629'),
       ('66620d36-6a37-4ce3-9db0-783ee3fd11b4', 'product-2', '2023-07-25 11:35:53.629');

-- INSERT INTO SAVING_ACCOUNTS (ACCOUNTNO, BALANCE, createdDate, closedDate,
--      interestStartDate, maturityDate, TYPE, STATUS, customerCifNo)
--  VALUES (1111, 12345123, '2023-07-25 11:34:53.629', '2023-07-25 11:34:53.629', '2023-07-25 11:34:53.629', '2023-07-25 11:34:53.629', '1', '11111', 123),
--     (1112, 123451 '2023-07-25 11:34:53.629', '2023-07-25 11:34:53.629', '2023-07-25 11:34:53.629', '2023-07-25 11:34:53.629', '1', '11112', 1234);